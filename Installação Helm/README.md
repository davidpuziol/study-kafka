# Instalação com o Helm

<https://artifacthub.io/packages/helm/bitnami/kafka>

<https://msazure.club/deploy-kafka-without-zookeeper/>

A primeira coisa que devemos fazer é criar um namespace para o kafka

```bash
kubectl create ns kafka
```

Agora vamos deployar o kafka usando com um chart do bitnami

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm pull bitnami/kafka --untar
# Ajuste as configurações do kafka no values.yaml antes de de aplicar
helm install kafka kafka/ -n kafka --wait
```

```bash
helm pull bitnami/schema-registry --untar
helm install schema-registry schema-registry/ -n kafka --wait
```

```bash
helm repo add kafka-ui https://provectus.github.io/kafka-ui-charts
helm pull kafka-ui/kafka-ui --untar
helm install kafka-ui kafka-ui/ -n kafka --wait
```

```bash
helm repo add kafka-ui https://provectus.github.io/kafka-ui-charts
helm pull kafka-ui/kafka-ui --untar
helm install kafka-ui kafka-ui/ -n kafka --wait
```

❯ helm repo add confluentinc https://confluentinc.github.io/cp-helm-charts

Para instalar outros componentes podemos usar alguns charts do confluent

Git clone https://github.com/confluentinc/cp-helm-charts/tree/master/charts
