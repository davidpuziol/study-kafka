# Study Kafka

<img src="./pics/kafka.png" alt="drawing" width="250"/>\

![kafka](./gif/kafka.gif)

[Documentação Oficial](https://kafka.apache.org/documentation/)

[Documentacao Confluent](https://docs.confluent.io/home/overview.html#confluent-documentation)

Inicialmente desenvolvido pelo LinkedIn em 2010 e agora está sendo usado por milhares de organizações. 80 entre as 100 maiores empresas utilizam kafka de diferentes formas.

O Apache Kafka é o software de processamento de stream open source mais popular para coletar, processar, armazenar e analisar dados em escala.

- Dxcelente desempenho
- Baixa latência
- Tolerância a falhas
- Alto rendimento
- Capaz de lidar com milhares de mensagens por segundo
- Escala horizontalmente (ilimitado) e com servidores de baixo custo
- Uso de hardwares mais baratos disponível no mercado e em cloud
- Não requer parada para upgrades, somente adicionar mais nós ao cluster

Alguns benefícios comuns são:

- Criação de pipelines de dados
- Alavancando fluxos de dados em tempo real, permitindo métricas operacionais e integração de dados em inúmeras fontes.

Kafka permite que as organizações modernizem suas estratégias de dados com arquitetura de streaming de eventos. As principais empresas do mundo utilizam Kafka.

## Casos de uso

- Transição de uma arquitetura monolítica para uma arquitetura de microserviços.
- Atua como uma Bus para eventos do sistema.
- Processamento assincrono porém em tempo real
- Para processar pagamentos e transações financeiras em tempo real, como em bolsas de valores, bancos e seguradoras.
- Para rastrear e monitorar carros, caminhões, frotas e remessas em tempo real, como na logística e na indústria automotiva.
- Para capturar e analisar continuamente os dados do sensor de dispositivos IoT ou outros equipamentos, como em fábricas e parques eólicos.
- Para coletar e reagir imediatamente às interações e pedidos do cliente, como no varejo, na indústria hoteleira e de viagens e em aplicativos móveis.
- Monitorar pacientes em atendimento hospitalar e prever mudanças nas condições para garantir tratamento oportuno em emergências.
- Conectar, armazenar e disponibilizar dados produzidos por diferentes divisões de uma empresa.
- Servir como base para plataformas de dados, arquiteturas orientadas a eventos e microsserviços.

## Kafka vs outros?

Várias clouds oferecem o kafka como serviço pronto para uso.


|                                | kafka                                                                                         | RabbitMQGenericName                                                                                                                |
|--------------------------------|-----------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| A linguagem é escrita em       | Java e Scala                                                                                  | Erlang                                                                                                                             |
| ano lançado                    | 2011                                                                                          | 2007                                                                                                                               |
| Protocolos                     | Mensagens binárias, int8, int16                                                               | AMQP por padrão                                                                                                                    |
| Popular para                   | Para reprodução de mensagens e fluxos de mensagens                                            | Agente de mensagens para aplicativos de comunicação em preto e branco                                                              |
| Retenção/exclusão de mensagem  | As mensagens são excluídas assim que o período de retenção termina.                           | Logo após os consumidores receberem a mensagem ou terminarem de processar e salvar os dados, a mensagem é apagada                  |
| Repetição da mensagem          | Sim, possível durante o período de retenção                                                   | Não, pois as mensagens são excluídas da fila imediatamente após a entrega                                                          |
| ACK de entrega                 | Sim,                                                                                          |                                                                                                                                    |
| Roteamento disponível          | Não possui algoritmos/regras de roteamento. Os tópicos definem a segregação necessária        | Vem com muitas regras de roteamento complexas. A troca e a ligação são usadas para enviar mensagens para as filas apropriadas      |
| Prioridade                     | Nenhum recurso de prioridade embutido.                                                        | Filas prioritárias são implementadas no local                                                                                      |
| Licença                        | Código Aberto: Licença Apache                                                                 | Código aberto: licença pública Mozilla                                                                                             |
| Projeto                        | broker burro, consumidor inteligente                                                        | broker esperto, consumidor burro                                                                                                 |
| Arquitetura                    | Produtor->troca->regras de vinculação->fila->consumidor                                       | Produtor->tópico sábio->corretor->partição->consumidor                                                                             |
| Caso de uso geral              | Operacional – operação do processo, registro.                                                 | Transacional – por exemplo, formação de pedido.                                                                                    |
| Fácil de lembrar               | Kafka é usado para Logging (desde sua capacidade de retenção de mensagens)                    | O RabbitMQ é idealmente usado como um intermediário de mensagens, que ajuda dois serviços/aplicativos diferentes a se comunicarem. |
| Dados de leitura do consumidor | Em um grupo de muitos consumidores pedindo para ler o mesmo tópico, apenas um terá permissão. | Mesmo                                                                                                                              |
| Filtrando mensagem             | Todas as mensagens do tópico assinado são enviadas ao consumidor.                             | Cabeçalhos de mensagens e troca de tópicos permitem que o consumidor seja seletivo ao receber apenas mensagens específicas         |
| Formato da mensagem            | Par de valor-chave, carimbo de data/hora                                                      | Cabeçalho e corpo                                                                                                                  |
| Autenticação                   | Kafka oferece suporte a Oauth2, autenticação padrão e Kerberos.                               | RabbitMQ suporta autenticação padrão e Oauth2.                                                                                     |
| Mantendo a ordem sequencial    | Kafka mantém offset para manter intacta a ordem de chegada das mensagens.                     | RabbitMQ implicitamente usa Queue que segue a propriedade FIFO e, portanto, mantém a ordem adequada das mensagens.                 |
| Bibliotecas suportadas         | Python, JAVA, Ruby, Node.JS                                                                   | Primavera, Swift. Python, PHP, .NET, C, Ruby                                                                                       |

A retenção de mensagens do Kafka faz com que ele se incline para um registrador de mensagens ou um servidor de streaming de dados enorme e rápido. Como a responsabilidade recai sobre o consumidor em tentar novamente uma mensagem após uma falha, Kafka não se importa se as mensagens são entregues com sucesso ou não. Isso elimina a implementação extra e o foco é colocado na reprodução e consulta de dados.

A falta de retenção de mensagens do RabbitMQ e a garantia de mensagens de confirmação dos consumidores o tornam mais adequado para ser um mediador de aplicativos, um corretor de mensagens robusto.

## Estudo

- Conceitos
- Arquitetura
- Desenvolvimento
- Ecossitema
- Instalação
