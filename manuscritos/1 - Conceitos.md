# Conceitos

## O que é streaming de eventos?

Streaming de eventos é o equivalente digital do sistema nervoso central do corpo humano. É a base tecnológica para um mundo "always-on", onde os negócios são cada vez mais definidos e automatizados por software, e onde o usuário do software é mais software.

Tecnicamente falando, streaming de eventos é a prática de capturar dados em tempo real de fontes de eventos como bancos de dados, sensores, dispositivos móveis, serviços em nuvem e aplicativos de software na forma de fluxos de eventos. Esses fluxos de eventos são então armazenados de forma durável para recuperação posterior, manipulados, processados e reagidos em tempo real ou retrospectivamente, e roteados para diferentes tecnologias de destino, conforme necessário. O streaming de eventos garante um fluxo contínuo e interpretação de dados para que a informação certa esteja no lugar certo, na hora certa.

O Kafka combina três recursos principais para implementar casos de uso para streaming de eventos de ponta a ponta com uma única solução testada em batalha:

- Publicação (producer) e consumo (consumer) de fluxos de eventos, incluindo importação/exportação contínua de dados de outros sistemas.
- Armazenamento durável e confiável de fluxos de eventos pelo tempo desejado.
- Processamento de fluxos de eventos à medida que ocorrem ou retrospectivamente.

Toda essa funcionalidade é fornecida de maneira distribuída, altamente escalável, elástica, tolerante a falhas e segura. O Kafka pode ser implantado em hardware bare-metal, máquinas virtuais e contêineres, tanto localmente quanto na nuvem. Você pode escolher entre autogerenciar seus ambientes Kafka e usar serviços totalmente gerenciados oferecidos por vários fornecedores.

## Vantagens no uso do kafka

- Os dados são publicados apenas uma vez.
- Os consumidores interessados nos dados podem assinar e consumir o que lhes interessa.
- Produtores e consumidores são desacoplados, podendo trabalhar em ritmos diferentes.
- Os consumidores podem ler dados mais de uma vez.
- A indisponibilidade do produtor não afeta o processo.
- Alta disponibilidade e capacidade com recursos de cluster e particionamento.

Se escalado verticalmente, surgiriam problemas como:

- Necessidade de interrupção para upgrade.
- Limite pequeno de escala.
- Requer configurações complexas.
- Problemas de incompatibilidade.

## O que abordar nesse estudo?

### Desenvolvimento

O Kafka oferece bibliotecas para a utilização de funções em praticamente todas as linguagens.

Um dos principais players que desenvolvem e até oferecem o Kafka como serviço é a [Confluent](https://developer.confluent.io/).

Veja aqui uma lista de bibliotecas e encontre a linguagem desejada, inclusive com exemplos de código para uso.

## Devops

Aqui é o foco principal do nosso estudo:

- Compreender a arquitetura do Kafka e provisioná-lo da melhor maneira possível.
- Entender os limites e qual configuração seria ideal para cada cenário.
