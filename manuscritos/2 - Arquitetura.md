# Arquitetura

O Kafka é um sistema distribuído que consiste em servidores e clientes que se comunicam por meio de um protocolo de rede TCP de alto desempenho.

>Pode ser implantado em hardware bare-metal, máquinas virtuais e contêineres no local, bem como em ambientes de nuvem.

- Servidores : Kafka é executado como um cluster de um ou mais servidores que podem abranger vários datacenters ou regiões de nuvem. Alguns desses servidores formam a camada de armazenamento, `chamados de brokers`. Outros servidores executam o Kafka Connect para importar e exportar continuamente dados como fluxos de eventos para integrar o Kafka com seus sistemas existentes, como bancos de dados relacionais, bem como outros clusters Kafka. Para permitir que você implemente casos de uso de missão crítica, um cluster Kafka é altamente escalável e tolerante a falhas: se algum de seus servidores falhar, os outros servidores assumirão seu trabalho para garantir operações contínuas sem perda de dados.

- Clientes : permitem escrever aplicativos e microsserviços distribuídos que leem, gravam e processam fluxos de eventos em paralelo, em escala e de maneira tolerante a falhas, mesmo no caso de problemas de rede ou falhas de máquina. O Kafka vem com alguns desses clientes incluídos, que são aumentados por dezenas de clientes fornecidos pela comunidade Kafka: os clientes estão disponíveis para Java e Scala, incluindo a biblioteca Kafka Streams de nível superior , para Go, Python, C/C++ e muitas outras programações idiomas, bem como APIs REST.

- Servidores: O Kafka é executado como um cluster de um ou mais servidores que podem abranger vários datacenters ou regiões de nuvem. Alguns desses servidores formam a camada de armazenamento, chamados de brokers. Outros servidores executam o Kafka Connect para importar e exportar continuamente dados como fluxos de eventos para integrar o Kafka com sistemas existentes, como bancos de dados relacionais, bem como outros clusters Kafka. Um cluster Kafka é altamente escalável e tolerante a falhas. Se um servidor falhar, outros assumirão seu trabalho para garantir operações contínuas sem perda de dados.

- Clientes: permitem escrever aplicativos e microsserviços distribuídos que leem, gravam e processam fluxos de eventos em paralelo, em escala e de maneira tolerante a falhas. O Kafka oferece vários clientes para diferentes linguagens de programação, como Java, Scala, Go, Python, C/C++, além de APIs REST.

![kafka-arch](../pics/kafka-arch.png)

## Componentes

- Kafka Broker
  - Cada nó do Kafka é chamado de broker. Um cluster Kafka possui vários brokers.
  - É possível ter vários brokers instalados na mesma instância, mudando apenas a porta, ou em servidores diferentes usando a mesma porta, mas com IPs diferentes.
  - Atribui numeração sequencial offset as mensagens
  - Salva as mensagens em disco
    - Não precisa ser consumido em tempo real
    - Permite recuperar falhas (consumidores)

- kafka client API
  - kafka connect
- kafka streams
- kafka ksql

## Evento

Um evento registra o fato de que "algo aconteceu" no mundo ou na sua empresa. Ao ler ou gravar dados no Kafka, você o faz na forma de eventos. Conceitualmente, um evento possui:

- uma chave
- valor
- timestamp
  - Como uma boa prática é utilizar sempre horário em UTC e deixar que quem for apresentar o dado trate.
- cabeçalhos de metadados opcionais.

> O evento é o dado em si, e muitas vezes conhecido como payload

Veja um exemplo de evento:

- Chave do evento: "Alice"
- Valor do evento: "Fiz um pagamento de $ 200 para Bob"
- Carimbo de data/hora do evento: "25 de junho de 2020 às 14h06"

Os eventos são organizados e armazenados de forma durável em tópicos. Imagine um forum de internet. Muitas regras podem ser definidas para um tópico, mas geralmente é N para N.

![event](../pics/event.png)

A key serve para separar o contexto. É possível criar um evento para um topico que vários consumers assinam. Porém um consumer pode especificar que só quer ler mensagem deste tópico com a key específica e descarta o restante. Um exemplo seria um topico TEMPERATURA e a key poderia ser o nome da cidade.

## Topico

>um tópico pode ter zero, um ou muitos produtores que gravam eventos nele, bem como zero, um ou muitos consumidores que assinam esses eventos.

Não existe limite para tópicos, pode ser criados quantos forem necessários.

Os eventos em um tópico podem ser lidos com a frequência necessária — ao contrário dos sistemas de mensagens tradicionais, os eventos não são excluídos após o consumo. Em vez disso, você define por quanto tempo o Kafka deve reter seus eventos por meio de uma definição de configuração por tópico, após a qual os eventos antigos serão descartados.

>O desempenho do Kafka é efetivamente constante em relação ao tamanho dos dados, portanto, armazenar dados por um longo período é perfeitamente aceitável.

Um evento em um tópico pode ser processado por vários consumers. Vamos imaginar que um evento precise depositar dinheiro na carteira do cliente e o mesmo evento precisa enviar uma notificação que o dinheiro foi depositado. Nesse caso existem configurações para o tópico que ele precisa ser lido por todos os consumers que assinam este tópico. Por isso o dado (evento, payload) não é deletado.

Um tópico na verdade é um grande log, pois podemos definir um tempo de retenção para esses eventos. Por default no kafka o padrão é de `1 semana`depois ele elimina o evento. É possível definir para quanto tempo quiser, e até mesmo pra sempre.

Voltando ao caso do topico temperatura e usar a key cidade, poderiamos sempre pegar o ultimo valor para uma cidade apenas do ultimo instante ao invés de ler todas as temperaturas. Esse recurso é chamado compact log. O kafka mantém a gravação de todos os dados, mas o consumer irá sempre no ultimo evento caso queira.

## Partições de um Tópico

Os tópicos são particionados, o que significa que um tópico é distribuído em vários "baldes" localizados em diferentes brokers no Kafka. Esse posicionamento distribuído de seus dados é muito importante para a escalabilidade porque permite que os aplicativos clientes leiam e gravem os dados de/para muitos intermediários ao mesmo tempo.

Quando um novo evento é publicado em um tópico, ele é realmente anexado `a uma das partições do tópico`. Eventos com a mesma chave de evento (por exemplo, um ID de cliente ou veículo) são gravados na mesma partição o garante que qualquer consumidor de uma determinada partição de tópico sempre lerá os eventos dessa partição `exatamente na mesma ordem em que foram gravados`.

>Mensagens sem key são distribuídas em partições de forma round robin.

Cada partição possuem seu sistema de fila FIFO, uma vez que sempre um mesmo key entra na mesma partição, garante-se a ordem de processamento. Cada posição dentro da partição, no caso cada caixinha é um `offset`da partição.

> Existe o offset 0 1 2 3 .. em todas as partições de todos os tópicos. Por causa desses offse separado, o kafka consegue guardar qual próxima mensagem cada consumer deve receber na ordem correta. Se um consumidor cair, o kafka sabe de onde ele parou.

Quando se cria um tópico no kafka é necessário escolher quantas partições esse tópico deve ter. Cada topico pode ter diferentes quantidades de partições, depende do volume eventos que será destinado a este tópico. Mais partições melhoram a leitura paralela por vários consumers.

> As partições de um tópico não precisam ficar necessáriamente no mesmo broker do clustter kafka

![topics-partitions](../pics/topics-partitions.png)

- Um topico com 4 partições
- Produtores publicam independente um do outro
- Quadrados iguais indicam eventos com o mesmo ID por isso são gravados na mesma partição.

Um detalhe muito importante é que podemos escolher um fator de replicação para cada tópido. Caso o fator de replicação seja 1 ele somente irá criar essa partição em algum broker e caso este broker quebre os dados serão perdidos. Se vc tem um cluster de 3 nodes por exemplo poderia replicar em todos os nodes com um fator de 3. Tudo depende da criticidade da informação.

Por isso é interessante ter pelo menos 2 replicas para cada tópico caso os dados sejam sensíveis.

Já que é possível ter replicas, uma das partições dentro de algum broker é considerada leader e as outras followers. `Sempre é lido da partição leader`. as partições followers somente ficam sincronizando informação e podem ser promovidas a leader caso caia o broker que tenha aquela partição leader. Ter 100 partições não aumenta a velocidade de leitura. Nesse caso é interessante aumentar consumers.

## Segmentos de uma partição de um tópico

Para trabalhar com diferentes retenções dos dados dos tópicos, cada partição divide-se em pequenos segmentos que são separações de arquivos para tratar do tempo de retenção.

## Productores e Consumidores

Os `produtores` são os aplicativos clientes que publicam (gravam) eventos no Kafka
Os `consumidores` são aqueles que assinam (leem e processam) esses eventos.

>Nada impede que um produtor também possa ser um consumidor. Um evento pode gerar outros eventos.

No Kafka, produtores e consumidores são totalmente desacoplados e agnósticos um do outro, o que é um elemento de design chave para alcançar a alta escalabilidade pela qual Kafka é conhecido.

Por exemplo, os produtores nunca precisam esperar pelos consumidores. O Kafka fornece várias garantias , como a capacidade de processar eventos exatamente uma vez.

>Um consumidor fica consumindo pra sempre, ou seja fica em um while true esperando chegando mensagem

![producer-consumer](../pics/producer-consumer.png)

Um outro detalhe importante é que um consumer pode ler(assinar) de vários tópicos ao mesmo tempo, não é obrigado ficar agarrado em um único tempo.

## Broker

O apache kafka é extremamente escalável e cada nó pertencente ao cluster é chamado de broker.
Cada broker tem seu próprio banco de dados que pode replicado em outros brokers para ter redundancia e escalabilidade.
Para controlar essa sincronia entre os brokers

## Boa prática

Para o timestamp utilize sempre em em UTC.
A camada de apresentação é quem tem que se preocupar com o formato e timezone exposto ao cliente.

>Uma configuração de produção comum é um fator de replicação de 3, ou seja, sempre haverá três cópias de seus dados. Essa replicação é realizada no nível das partições de tópico.
