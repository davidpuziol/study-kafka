# Desenvolvimento

Algumas coisas que desenvolvedores devem conhecer além dos conceitos principais.

## Etapas de produção de uma MSG

![Message](../pics/message.png)

Assim que for escolhido o tópico e temos o valor, a chave é opcional para garantir a ordem e a partição correta, existirá um processo de serialização.

Práticamente envia-se texto simples como json na maioria dos casos, mas é bastante interessante estudar sobre o [AVRO](https://avro.apache.org/) que é um jeito especial de dar tipo aos campos do Json. Afinal é possível ler `true` do outro lado como string ao invés de booleano.

Escolhendo a partição de acordo com a chave ou round robin e irá colocar no broker correto. Não é necessário se preocupar nesse momento com definir replicas para o tópico, quantidade de partição, etc pois isso está em um momento de criação dos tópicos e não no envio da mensagem.

> É possível em uma configuração do kafka criar tópicos caso não existam e preparar algumas definições default para ele.

## Garantia de entrega

Existem 3 formatos de entrega da mensagem:

- Enviar uma msg que pode ser perdida no caminho.
  - Ex: Localização do motorista do uber. Voce pode perder pq envia uma msg a cada 2 segundos, logo se vc receber 8 de 10 tá ótimo.
  - Neste caso o ACK = 0. Envia-se a msg e não precisa de confirmação da entrega.
  _ Mais velocidade ao custo perdas

- Enviar uma msg que pode ser lida mais de uma vez que não faz diferença
  - ex: Temperatura da cidade
  - Neste caso o ACK =1. Pelo menos uma confirmação espera receber, mesmo que seja mais que uma.
  - Mais velocidade moderada com garantia de entrega

- Garantia que recebeu somente 1 vez.
  - ex: deposito ou saque bancário.
  - Neste caso o ACK = -1. Nesse caso somente 1 mensagem é enviado e espera-se a replicação em todos as partições que devem ser replicadas em todos os broker e uma ÚNICA confirmação de entrega.
  - Garantia total ao custo de velocidade e perfomance
  - I

> Trabalhe com as opções de garantia com bom senso!

## Idepotencia

Existe uma parametro chamado Idempotent que pode ser ON ou OFF no produtor para garantir exatamente uma entrega uma entrega da msg.

O kafka consegue descartar mensagens repetidas utilizando o timestamp e garantir a ordem correta.

## Consumer group

É possível ter um grupo de consumidores lendo os mesmo tópicos. O kafka gerencia isso facilitando o paralelismo. No caso se um topico tem 3 partições e vc tem um consumer group com 3 consumers ele dividiria um consumer para ler cada uma das partições. No caso se tivesse somente 2 consumers algum deles ia ler duas partições.

`Quando conecta um consumer a um cluster somente seria necessário avisar a qual consumer group ele pertente que ele já assinaria os tópicos automaticamente`.

Se vc tiver 3 consumers para duas partições, um consumer ficaria parado. Nesse caso o correto é tenta manter um consumer para cada partição para ter o máximo de aproveitamento

No caso de um consumer cair, o kafka faz o rebalance automaticamente sobrecarregando algum consumer para ler mais de uma partição caso necessário.

## Segurança

É possível encriptar todas as messagens em transito entre produto <--> kafka <--> consumer, mas os dados são gravador nas partições sem encriptação. O kafka só garante a transito dos dados criptografados.

Se for necessário gravar dados criptografados crie uma msg já criptografada, e somente quem tem a chave conseguiria descriptografar

## Autenticação e authorização

É possível definir quem pode ter acesso ao cluster para produzir msg ou consumir.
Inclusive quais tópicos um consumidor pode ler e quais tópidos um produtor pode escrever.

## Compatibilidade de Dados

Kafka, em sua essência, apenas transfere dados em formato de byte. Não há nenhuma verificação de dados sendo feita no nível do cluster Kafka. Na verdade, o Kafka nem sabe que tipo de dados está enviando ou recebendo; se é uma string ou inteiro.

![kafka-tranfer](../pics/kafka-transfer.png)

Devido à natureza desacoplada de Kafka, produtores e consumidores não se comunicam diretamente, mas a transferência de informações ocorre por meio do tópico Kafka. Ao mesmo tempo, o consumidor ainda precisa saber o tipo de dados que o produtor está enviando para desserializá-lo. Imagine se o produtor começar a enviar dados inválidos para o Kafka ou se o tipo de dados dos seus dados for alterado. Seus consumidores downstream começarão a quebrar. Precisamos de uma maneira de ter um tipo de dados comum que deve ser acordado.

É aí que o **Schema Registry** entra em cena. É um aplicativo que `reside fora do cluster Kafka` e manipula a distribuição de esquemas para o produtor e o consumidor armazenando uma cópia do esquema em seu cache local.

![schema-reg](../pics/kafka-schema-registry.png)

Com o registro do esquema instalado, o produtor, antes de enviar os dados para o Kafka, conversa primeiro com o registro do esquema e verifica se o esquema está disponível. Se ele não encontrar o esquema, ele o registrará e o armazenará em cache no registro do esquema. Assim que o produtor obtiver o esquema, ele serializará os dados com o esquema e os enviará para Kafka em formato binário anexado com um ID de esquema exclusivo. Quando o consumidor processar esta mensagem, ele se comunicará com o registro do esquema usando o ID do esquema obtido do produtor e o desserializará usando o mesmo esquema. Se houver uma incompatibilidade de esquema, o registro do esquema emitirá um erro informando ao produtor que está violando o acordo do esquema.

Que tipo de formato de serialização de dados devemos usar com Schema Registry?
Opontos importantes que devemos considerar ao escolher o formato de serialização de dados correto:

- Se o formato de serialização for binário
  - O formato binário é, obviamente, mais compactado, então o uso de armazenamento será menor
  - Formatos de texto simples, especialmente JSON, têm alguns problemas com precisão numérica
- Se pudermos usar esquemas para impor estrutura de dados escritas.

[IDL](https://en.wikipedia.org/wiki/Interface_description_language) (Interface Description Language) define padrões de mensagem

| Nome | Binario | IDL | DESENVOLVEDOR | OBSERVACAO |
|---|---|---|---|---|
| JSON | NO | YES || Não tipado |
| YAML | NO | NO || Não tipado |
| XML | NO | YES | W3C | Muito Verboso |
| kyro | NO | YES | Esoteric Software | Só funciona em JVM |
| AVRO | YES | YES | Apache |Padrão da Confluent e maior curva de aprendizagem |
| Protocol Buffer | YES | YES | Google | Fácil de usar |
| Thrift | YES | YES | Facebook | Quase o mesmo que protocol buffer e mais difícil de usar |

Alguns benchmarks:

Velocidade
![speed](../pics/schema-bench-1.png)

Tamanho
![speed](../pics/schema-bench-2.png)

## AVRO

Um exemplo de um schema avro:

```json
{
  "namespace": "pnda.entity",
  "type": "record",
  "name": "event",
  "field": {
    {"name": "timestamp":, "type": "long"},
    {"name": "scr",        "type": "string"},
    {"name": "host_ip":,   "type": "string"},
    {"name": "rawdata": ,  "type": "bytes"}
  }
}
```

Características

- Os formatos de dados são descritos escrevendo Avro Schemas , que estão em JSON.
- Os dados podem ser usados ​​de forma genérica como Generic Records ou com código compilado.
- Os dados salvos em arquivos ou transmitidos por uma rede geralmente contêm o próprio esquema.

Prós

- Formato binário eficiente que reduz o tamanho dos registros.
- Os esquemas do leitor e do gravador são conhecidos, permitindo grandes alterações no formato dos dados.
- Linguagem muito expressiva para escrever esquemas.
- Nenhuma recompilação é necessária para suportar novos formatos de dados.

Contras

- O formato Avro Schema é complexo e requer estudo para aprender.
- As bibliotecas Avro para serialização e desserialização são mais complexas de aprender.

Útil quando:

- É muito importante minimizar o tamanho dos dados.
- Os formatos de dados estão em constante evolução ou até mesmo algo que os usuários podem usar.
- São necessárias soluções genéricas para problemas como armazenamento de dados ou sistemas de consulta de dados.

## Protocol Buffer

Caracteristicas:

- Os formatos de dados são descritos escrevendo arquivos proto em um formato personalizado.
- Um compilador protobuf gera código na linguagem de programação escolhida.
- Você cria, serializa e desserializa seus dados usando o código gerado.

Prós

- Formato binário muito eficiente que reduz bytes.
- Representação JSON padrão.
- Código gerado fortemente tipado para acelerar o desenvolvimento.

Contras

- Requer recompilação quando os formatos mudam.
- É necessária uma disciplina rigorosa para manter a compatibilidade com versões anteriores quando os formatos mudam.

Útil quando:

- É muito importante minimizar o tamanho dos dados
- Os formatos de dados não mudam com muita frequência.

---
> Eu particulamente gosto mais do AVRO apesar de dar mais trabalho, não é necessário um compilador.
