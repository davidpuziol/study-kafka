# Ecossitema

O kafka possui todo um ecossitemas em volta dele que não necessáriamente estão presentes dentro do cluster kafka, mas fazem comunicação com ele.

![kafka-connector](../pics/kafka-connectors.png)

Esses recursos já existem, evitando ter que desenvolver sistemas novos a não ser que seja preciso.

Sao divididos em dois tipos:

- Kafka connect
- Kafka sync

Ambos podem ser clusters, assim como o kafka.
Muitas vezes a quantidade de dados é tanta que para dar vazão é necessário subir vários workers para um cluster de um connect ou sync específico.

> Se vc está usando o kafka para trazer informação de algum lugar e enviar para outro, o ideal é manter a vazão. Se tem 3 tasks de entrada tenha 3 de saída.

## Kafka connect

Os conectores Kafka são componentes conectáveis ​​responsáveis ​​pela interface com sistemas de dados externos para facilitar o compartilhamento de dados entre eles e o Kafka. Eles simplificam o processo de importação de dados de sistemas externos para Kafka e exportação de dados de Kafka para sistemas externos.

Você pode usar conectores Kafka existentes para fontes de dados e coletores comuns ou até mesmo implementar seus próprios conectores. Um conector de origem é usado para coletar dados de sistemas como bancos de dados, tabelas de fluxo, corretores de mensagens, etc., e um conector de coletor é usado para fornecer dados a sistemas de lote, índices ou qualquer tipo de banco de dados.

Há muitos conectores disponíveis para mover dados entre Kafka e outros sistemas de dados populares, como S3, JDBC, Couchbase, S3, Golden Gate, Cassandra, MongoDB, Elasticsearch, Hadoop e muitos mais.

- Auxilia no desenvolvimento, implantação e gerenciamento de Conectores Kafka, facilitando o estabelecimento de conexões com sistemas externos.
- Vem com uma interface REST que permite gerenciar e controlar conectores usando uma API REST.
- Projetado para ser escalável e tolerante a falhas, o que significa que você pode implantar o Kafka Connect não apenas como um processo individual, mas também como um cluster.
- Automatiza o processo de confirmação de deslocamento , o que evita o trabalho de implementar manualmente esse processo tedioso e sujeito a erros.
- Processamento de dados em lote.

> O kafka connect não pertence ao cluster do kafka, são software indepentes com facilidades para enviar dados para o kafka.

## Kafka Sync

Faz exatamente o contrário do connector que tem a função de trazer dados para dentro do kafka, este envia os dados do kafka para algum outro lugar.

[Lista de alguns](https://www.confluent.io/product/connectors/?utm_medium=sem&utm_source=google&utm_campaign=ch.sem_br.nonbrand_tp.prs_tgt.kafka-connectors_mt.xct_rgn.latam_lng.eng_dv.all_con.kafka-connectors&utm_term=kafka+connect+hub&placement=&device=c&creative=&gclid=Cj0KCQiAgOefBhDgARIsAMhqXA6KOtSW0-RnK0RDOIvoJa0ZwIcFPJ4dgTR-e3IOlZ9XfWS_3i7k5n8aArUXEALw_wcB), mas outros podem ser encontrados tambẽm

## Kafka Rest Proxy

Para situações em que não existe uma biblioteca para a linguagem, suporte ao driver do kafka ou alguma outra situação diferente, existe uma api rest que faz o serviço de enviar para o kafka a mensagem.

Muitas vezes quando não se quer programar a função que envia a mensagem para um topico do kafka pode se usar este recurso.

## Kafka Streams

Extremamente útil.

- É uma biblioteca em Java que ajuda a processar e tranformar dados.
- Garante o Exacly Once ou seja, entregue apena 1 vez.
- Escalável
- Tolerante a falhas.
- Consegue agrupar informação (tirar média)
- Processamento em tempo real

É necessário um consumer produzido na linguagem Java e utilize essa biblioteca para processar os dados e transformá-los em um formato melhor que você precisa para trabalhar. Depois de consumido ele produzirá uma outra mensagem para algum outro tópico que irá consumir essa msg. 

> Ao mesmo tempo ele é producer e consumer

## KSQL (Kafka SQL)

É uma linguagem que possibilita rodar comandos SQL nos tópicos (filas).
Seria como tratar as mensagem como um banco de dados, pois geralmente uma mensagem em um tópico específico possuí um padrão definido em um schema, como por exemplo o [AVRO](./3%20-%20Desenvolvimento.md/#AVRO)
